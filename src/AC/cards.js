import { CARDS, LOAD } from '../constants';

export function loadCards(query) {
  return {
    type: CARDS + LOAD,
    api: 'http://pokeapi.co' + query
  };
}
