import React, {Component} from 'react';
import {connect} from 'react-redux';
import RouterAccess from '../decorators/RouterAccess';

const accepted = ['attack', 'defense', 'hp', 'sp_atk', 'sp_def', 'speed'];

class Card extends Component {

  handleClick = () => {
    this.props.router.push('/cards');
  }

  render() {
    const { height, name, pkdx_id: id, sprite, types, weight } = this.props.entity;
    const { table } = this.props;

    const progressBody = table.map(e => {
      const { name, value } = e;
      return (
        <li className="list-group-item" key={name}>
          <h5 className="progress-description">{name}</h5>
          <progress className="progress" value={value} max="100" />
          <span className="progress-value">{value}</span>
        </li>
      );
    });
    const typesBody = types.map((t, i) => (
      <span key={id+i} onClick={this.handleClick} className={`card-pokemon_type card-pokemon_type-${t.name}`}>
        {t.name}
      </span>
    ));

    return (
      <div className="card card-pokemon">
        <div className="card-header">
          <h3 className="card-pokemon_header-h">{name}</h3>
          {typesBody}
          <div className="card-pokemon_header-button" onClick={this.handleClick} />
        </div>
        <div className="card-block">
          <div className="col-xs-6">
            <div className="card-block card-pokemon_sprite-container">
              <img src={sprite} alt="pokemons appearance" className="card-pokemon_sprite-item"/>
            </div>
            <div className="card-block card-pokemon_hw-container">
              <div className="col-xs-6 card-pokemon_hw-item">
                <h4>Height</h4>
                <p>{height}</p>
              </div>
              <div className="col-xs-6 card-pokemon_hw-item">
                <h4>Weight</h4>
                <p>{weight}</p>
              </div>
            </div>
          </div>
          <div className="col-xs-6">
            <ul className="list-group">
              {progressBody}
            </ul>
          </div>
        </div>
      </div>
    );
  }

}

export default connect(({cards}, props) => {
  const { pathname } = props.location;
  const name = `${pathname.split('/').slice(-1)}`;
  const entity = cards.getIn(['entities', name]);
  const table = createTable(entity);
  return {entity, table};
}, {})(RouterAccess(Card));

function createTable(object) {
  let table = [];
  for (const [key, value] of Object.entries(object)){
    if ( accepted.some(e => e === key) ) {
      table.push({
        name: key === 'sp_atk' ? 'special attack' : key === 'sp_def' ? 'special  defense' : key,
        value
      });
    }
  }
  return table;
}
