import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';

import {loadCards as load} from '../AC/cards';

import CardsListItem from '../components/CardsListItem';
import InfiniteLoad from '../decorators/InfiniteLoad';
import Spinner from '../components/Common/Spinner';


class CardsList extends Component {

  componentDidMount() {
    const {load, api, items} = this.props;
    if ( !items.count() ) load(api);
  }

  setReady= () => {

    const list = ReactDOM
      .findDOMNode(this.refs.list)
      .querySelectorAll('.card-pokemon_sprite');

    for ( const image of list ) {
      if ( !image.complete ) return;
    }

    this.props.setReady();
  }

  render() {

    const { items, loading } = this.props;

    if ( !items.count() ) return <Spinner />;

    const button = loading ? <Spinner /> : <div style={{marginTop: '100px'}} />;
    const listBody = items.map(i => {
      const { name, sprite, types, pkdx_id } = i;
      return <CardsListItem
        key={pkdx_id}
        data={{name, sprite, types, pkdx_id}}
        onImageLoad={this.setReady}
        />;
    });

    return (
      <div className="row" id="cards">
        <div className="col-xs-12" ref="list">
          {listBody}
        </div>
        <div className="container-buttons col-xs-12">
          {button}
        </div>
        <div id="card" className="col-xs-12 col-lg-10">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default connect(({cards}) => ({
  api    : cards.get('next'),
  items  : cards.get('entities').valueSeq(),
  loading: cards.get('loading')
}), {load})(InfiniteLoad(CardsList));
