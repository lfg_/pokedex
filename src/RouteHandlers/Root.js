import React from 'react';

const Root = (props) => {
  return (
    <div>
      <img src="https://i.imgur.com/1jBybmE.png" alt="Pokemon Logo" id="logo"/>
      {props.children}
    </div>
  );
};

export default Root;
