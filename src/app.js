import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import routes from './routes';
import store from './store';

import './styles/styles.sass';
import 'bootstrap/dist/css/bootstrap.css';

const App = () => {
  return (
    <Provider store={store}>
      {routes}
    </Provider>
  );
};

render(<App />, document.getElementById('container'));
