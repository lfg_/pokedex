import React, {Component} from 'react';
import RouterAccess from '../decorators/RouterAccess';

class CardsListItem extends Component {

  handleClick = () => {
    const name = this.props.data.name.toLowerCase();
    this.props.router.push(`/cards/${name}`);
  }

  render() {
    const { name, sprite, types, pkdx_id: id } = this.props.data;
    const { onImageLoad } = this.props;
    const typesBody = types.map((t, i) => (
      <span key={id+i} onClick={this.handleClick} className={`card-pokemon_type card-pokemon_type-${t.name}`}>
        {t.name}
      </span>
    ));

    return (
      <div className="col-xs-4 card-pokemon card-pokemon-of-list" onClick={this.handleClick}>
        <div className="card">
        <div className="card-block card-pokemon_sprite-container">
          <img
            src={sprite}
            alt="pokemons appearance"
            onLoad={onImageLoad}
            className="card-img-top card-pokemon_sprite-item"/>
        </div>
        <div className="card-block">
          <h3 className="card-title">{name}</h3>
          {typesBody}
        </div>
        </div>
      </div>
    );
  }

}

export default RouterAccess(CardsListItem);
