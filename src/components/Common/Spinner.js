import React from 'react';

const Spinner = () => {
  return (
    <div id="spinner">
      <div className="spinner-inner">
        <div className="spinner-white" />
      </div>
      <div className="spinner-outer">
        <div className="spinner-white" />
      </div>
    </div>
  );
};

export default Spinner;
