export const API_URL = '/api/v1/pokemon/?limit=9';

export const CARDS   = 'CARDS';

export const LOAD    = '_LOAD';

export const START   = '_START';
export const SUCCESS = '_SUCCESS';
export const FAIL    = '_FAIL';
