import React from 'react';
import ReactDOM from 'react-dom';

export default (Component) => {

  return class InfiniteLoad extends React.Component {

    componentDidUpdate() {

      const node = ReactDOM.findDOMNode(this.refs.infiniteload);
      const { ready } = this.state;
      if ( !node || !ready ) return;

      const offset =
        calculateOffset(node);

      if ( offset !== this.state.offset ) this.setState({offset});
      else this.attachListener();

    }

    state = {
      offset: null,
      ready : false
    }

    setReady = () => {
      this.setState({ready: true});
    }

    attachListener = () => {
      window.addEventListener('scroll', this.load);
    }

    detachListener = () => {
       window.removeEventListener('scroll', this.load);
    }

    load = () => {
      const scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
      if ( window.innerHeight + scrollTop >= this.state.offset ) {
        this.detachListener();
        this.setState({ready: false});
        const { api, load } = this.props;
        load(api);
      }
    }

    render() {
       return (
        <Component
          { ...this.props } ref="infiniteload" setReady={this.setReady}/>
      );
    }

  };

};


function calculateOffset(ref) {
  const { offsetHeight, offsetTop } = ref;
  return offsetTop + offsetHeight;
  //ref.getBoundingClientRect().bottom?
}
