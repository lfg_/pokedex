import React, {Component, PropTypes} from 'react';

export default (Decoratee) => {
  return class RouterAccessDecorator extends Component {
    static contextTypes = {
      router: PropTypes.object
    }
    render() {
      return (
        <Decoratee {...this.props} {...this.context} />
      );
    }
  };
};
