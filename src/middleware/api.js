import axios from 'axios';
import {START, SUCCESS, FAIL} from '../constants';

export default () => next => action => {

  const { api, type } = action;

  if ( !api ) return next(action);

  next({type: type + START});

  axios
    .get(api)
    .then(res => {
      next({type: type + SUCCESS, res});
    })
    .catch(err => {
      next({type: type + FAIL, err});
    });

};
