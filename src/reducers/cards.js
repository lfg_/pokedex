import { CARDS, LOAD, START, SUCCESS, API_URL } from '../constants';
import { Map, OrderedMap, Record } from 'immutable';
import { arrayToMap } from '../utilities';

const cardModel = new Record({
  name   : '',
  types  : [],
  attack : null,
  defense: null,
  hp     : null,
  sp_atk : null,
  sp_def : null,
  speed  : null,
  weight : '',
  height : '',
  pkdx_id: null,
  sprite : ''
});

const defaultState = new Map({
  entities: new OrderedMap({}),
  loading: false,
  next: API_URL
});


export default (state = defaultState, action) => {

  const { type, res } = action;

  switch(type) {
    case CARDS + LOAD + START:
      return state.set('loading', true);
    case CARDS + LOAD + SUCCESS:
      return state
        .updateIn(['entities'], e =>
          e.merge(arrayToMap(res.data.objects, cardModel)))
        .set('next', res.data.meta.next)
        .set('loading', false);

  }

  return state;

};
