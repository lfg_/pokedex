import Immutable from 'immutable';
// see dev note on line 52
export const loadState = () => {
  try {
    const serialized = localStorage.getItem('state');

    if ( serialized === null ) {
      return {};
    } else {

      const parsed = JSON.parse(serialized);

      let immutable = {};

      for (let [key, val] of Object.entries(parsed)) {
        immutable[key] = Immutable.fromJS(val,  (key, value) => {
          if ( key === '' ) {
            return value.toMap();
          } else if ( key === 'entities' ) {
            return value.toOrderedMap();
          } else {
            return value.toJS();
          }
        });
      }

      return immutable;

    }

  } catch (err) {
    return {};
  }
};

export const saveState = (state) => {
  try {
    let mutable = {};

    for (let [key, val] of Object.entries(state)) {
      mutable[key] = val.toJS();
    }

    const serialized = JSON.stringify(mutable);
    localStorage.setItem('state', serialized);

  } catch (err) {
    //do nothing;
  }
};

// Developer note:
// there was no reason behind sticking to the state
// shaped like jsObject {                     *** see line 21
//  key: immutableMap {} etc
// }
// ( besides a slightly increased convenience in
//   mapstatetoprops destructuring )
// I could have done it like this: immutableMap {
//    key: immutableMap {} etc
//  }
// and avoid excessive loops and declaration of new variables
// nevertheless, i'm giving it a go because
// life is pain.
// ( and artificial obstacles do exist in the wild
//   due to whatever reason
//   and i better be preparing for it ) 
