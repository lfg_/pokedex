import React from 'react';
import { Router, Route, browserHistory, IndexRedirect } from 'react-router';

import Root from './RouteHandlers/Root';
import CardsList from './RouteHandlers/CardsList';
import Card from './RouteHandlers/Card';

export default (
    <Router history = {browserHistory}>
        <Route path = "/" component = {Root} >
            <IndexRedirect to = "/cards" />
            <Route path = "cards" component = {CardsList}>
              <Route path =":name" component = {Card} />
            </Route>
        </Route>
    </Router>
);
