import { createStore, applyMiddleware, compose } from 'redux';

import reducer from '../reducers';
import { loadState, saveState } from '../reducers/localStorage';

import api from '../middleware/api';
import createLogger from 'redux-logger';

const defaultState = loadState();
const enhancer = compose(
    applyMiddleware(api, createLogger({collapsed:true})),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(reducer, defaultState, enhancer);
store.subscribe(() => {
  saveState(store.getState());
});


window.store = store;

export default store;
