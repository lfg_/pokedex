import { OrderedMap } from 'immutable';

export function arrayToMap(array, RecordModel) {
    return array.reduce((acc, item) => {
        return acc.set(item.name.toLowerCase(),
          new RecordModel(Object.assign(item, {sprite: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${item.pkdx_id}.png`})));
    }, new OrderedMap({}));
}
