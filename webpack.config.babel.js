/*eslint no-unused-vars:0*/
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';

export default {

    entry: [
      'babel-polyfill',
      './src/app.js'
    ],

    output: {
      path: './public',
      publicPath: '/',
      filename: 'bundle.js'
    },

    watch: true,

    watchOptions: {
      aggregateTimeout: 100
    },

    devServer: {
      historyApiFallback: true,
      contentBase: './'
    },

    devtool: 'cheap-module-eval-source-map',

    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loaders: ['babel', 'eslint']
        },
        {
          test: /\.(scss|sass)$/,
          loader: ExtractTextPlugin.extract('style', ['css', 'postcss', 'sass'])
        },
        {
          test: /\.css$/,
          loader: ExtractTextPlugin.extract('style', ['css', 'postcss'])
        }

      ]
    },

    plugins: [
      new ExtractTextPlugin('styles.css')
    ],

    postcss: () => [autoprefixer]
};
